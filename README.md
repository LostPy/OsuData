# OsuData

<img align="left" width="100" height="100" src="https://www.python.org/static/img/python-logo-large.c36dccadd999.png"> <img align="right" width="100" height="100" src="https://gblobscdn.gitbook.com/spaces%2F-MjyihBKwzw5hqd3AIyq%2Favatar-1632073117169.png?alt=media">

A small framework to work or vizualise osu! beatmaps data.  
This framework use object-oriented programming (OOP) to easily manage beatmap data.
You can use `export` and `info` modules to work without object-oriented programming.  
The documentation is in [wiki of this GitLab](https://lostpy.gitbook.io/osudata/).

## Index <a id="index"></a>
 1. [Global informations](#globalInfos)
 2. [Requirements](#requirements)
 3. [Installation](#installation)
 4. [Models](#models)

## Global informations <a id="globalInfos"></a>
 * Author: [LostPy][me]

 * Version: 4
 
 * License: [MIT License][license]

 * Useful links:
   * [osu! .osu file format][osu_format]
   * [osu! .db file format][osu_db_format]

 * [**Documentation**](https://lostpy.gitbook.io/osudata/)

## Requirements: <a id="requirements"></a>
### Mandatory
* [Python 3.x][py]
* [peace-performance-python][pppy] (To get data from .osu file)
* [pydub][pydub] (To manipulate mp3 file and play music)
* [numpy][np] (Basis for data manipulation)
* [scipy][scipy] (To extract music data)
* [pandas][pd] (Basis for data manipulation)

### Optionnal
* [colorama][color] (For coulour logs)
* [plotly][plotly] or [seaborn][seaborn] (To visualize data)
* [openpyxl][pyxl] (To export DataFrame in .xlsx with pandas)

## Installation <a id="installation"></a>
To install this package, you can use the following command:

`pip install git+https://gitlab.com/LostPy/OsuData.git@main`

**Don't forget to install [dependencies](#requirements).**

**To update** the package, you can use:  
`pip install git+https://gitlab.com/LostPy/OsuData.git@main --upgrade`

OR

`pip install git+https://gitlab.com/LostPy/OsuData.git@main -U`


[py]: https://www.python.org/
[pppy]: https://github.com/Pure-Peace/peace-performance-python
[color]: https://pypi.org/project/colorama/
[pydub]: https://github.com/jiaaro/pydub
[np]: https://numpy.org/
[scipy]: https://www.scipy.org/docs.html
[pd]: https://pandas.pydata.org/
[pyxl]: https://openpyxl.readthedocs.io/en/stable/
[pdDf]: https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html
[pdToExcel]: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_excel.html
[plotly]: https://plotly.com/
[seaborn]: https://seaborn.pydata.org/
[osu_format]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29
[structure]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29#structure
[general]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29#general
[metadata]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29#metadata
[difficulty]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29#difficulty
[hit-objects]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Osu_%28file_format%29#hit-objects
[osu_db_format]: https://osu.ppy.sh/wiki/en/osu%21_File_Formats/Db_%28file_format%29
[license]: https://github.com/LostPy/osuData/blob/main/LICENSE
[me]: https://osu.ppy.sh/users/11187592
